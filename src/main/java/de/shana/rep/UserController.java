package de.shana.rep;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.shana.rep.*;

@RestController

public class UserController {

	@Autowired
	UserRepoImpl user;
	@RequestMapping(value="/user",method=RequestMethod.GET,produces="application/json")
	public ResponseEntity<String> getUser()
	
	{
		List<User> uservalues=user.getUser();
		ResponseEntity<String> resp=new ResponseEntity<String>(uservalues.toString(),HttpStatus.OK);
		
	  //  return user.getUser();
		return resp;
	}

}
