package de.shana.rep;

import java.util.ArrayList;
import java.util.List;

public interface UserRepo {
	
	public List<User> getUser(); 

}
