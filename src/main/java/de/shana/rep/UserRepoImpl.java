package de.shana.rep;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

@Repository
public class UserRepoImpl implements UserRepo {

	public List<User> getUser() {
		User u=new User();
		u.setAge("12");
		u.setName("sahana");
		
		User u1=new User();
		u1.setAge("34");
		u1.setName("gautham");
		
		List<User> list=new ArrayList<User>();
		list.add(u1);
		list.add(u);
		return list;
	
	}

}
