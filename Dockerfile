FROM tomcat:8-jre8

MAINTAINER Gautham

#COPY ~ C:\Users\sahagau\Documents\Sample_maven_gauthams_pro.war /opt/tomcat/webapps/

#COPY ./Sample_maven_gauthams_pro.war /usr/local/tomcat/webapps/

#COPY cd /var/lib/jenkins/workspace/Sample_Maven_project/target/Sample_maven_gauthams_pro.war/ /usr/local/tomcat/webapps/

COPY ./target/Sample_maven_gauthams_pro.war/ /usr/local/tomcat/webapps

#COPY /var/lib/jenkins/workspace/Sample_Docker_Project/target/*.war /usr/local/tomcat/webapps/

EXPOSE 8081
